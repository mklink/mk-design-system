declare function useEventCallback<T extends (...args: any[]) => any>(fn: T): T;
export default useEventCallback;
