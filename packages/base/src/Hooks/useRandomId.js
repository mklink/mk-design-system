import * as React from 'react';

let currentId = 0;

function getRandomId(prefix = 'orion') {
  currentId += 1;
  return `${prefix}-${currentId}`;
}

export default function useRandomId(id, prefix) {
  const idRef = React.useRef(id || getRandomId(prefix));
  return idRef.current;
}

/**
 * Put it on root of the app to reset id on each app render.
 * (otherwise server would keep increasing it with each request
 * and cause client-server markup mismatch)
 */
export function ResetHtmlIdGenerator() {
  currentId = 0;
  return null;
}
