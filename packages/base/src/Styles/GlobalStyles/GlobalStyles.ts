/* ============================================================================
Globally Shared Styles
Styles that are applied to all components and elements,
creating a style base to start at.
 ============================================================================== */
import { createGlobalStyle } from 'styled-components';

/* eslint no-unused-expressions: 0 */
const GlobalStyles = createGlobalStyle`
  /* this removes warning from reach-ui about missing styles */
  :root {
    --reach-dialog: 1;
    --reach-menu-button: 1;
  }

  html,
  body {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
  }

  html * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
  }

  body {
    font-family: ${props => props.theme.fonts.primaryText};
    line-height: 1.5;
  }

  /* setting reset of browser default - if set in components more prone to issues with overriding - also the reason to not use shorthand */
  p, h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 0;
    padding-bottom: 0;
  }

  .visually-hidden {
    /* Use to hide something visually but still make it keyboard accessible */
    border: 0;
    clip: rect(0 0 0 0);
    height: auto;
    margin: 0;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
    white-space: nowrap;
  }
`;

export default GlobalStyles;
