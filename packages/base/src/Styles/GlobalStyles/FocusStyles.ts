import { css } from 'styled-components';

const FocusStyles = css`
  /* Keyboard only focus styles. */
  &.focus-visible {
    outline: 2px solid ${props => props.theme.colors.focusRingColor};
  }
`;

export default FocusStyles;
