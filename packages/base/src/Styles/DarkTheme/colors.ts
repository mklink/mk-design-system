// color names and values from design
import baseColors from '../Base/colors';

const aliases = {
  // aliases
  primaryTextColor: baseColors.dark300,
  secondaryTextColor: baseColors.gray400,
  /* Primary and Secondary Colors */
  primaryColor: baseColors.primaryBrand300,
  secondaryColor: baseColors.dark100,
  /* Hover */
  hoverColor: baseColors.primaryBrand500,
  hoverTextColor: baseColors.white,
  hoverOutlinedBGColor: baseColors.light100,
  /* Focus */
  focusColor: baseColors.primaryBrand500,
  focusTextColor: baseColors.white,
  focusOutlinedBGColor: baseColors.light100,
  /* Active */
  activeColor: baseColors.primaryBrand600,
  activeTextColor: baseColors.white,
  activeOutlinedBGColor: baseColors.light200,
  /* Focus Outline */
  focusRingColor: baseColors.lightAccent100,
  /* error */
  errorColor: baseColors.dangerColor,
  /* Disabled Colors */
  disabledColor: baseColors.gray100,
  disabledTextColor: baseColors.gray300,
  /* Borders */
  borderColor: baseColors.gray300,
};

export default {
  ...baseColors,
  ...aliases,
};
