import breakpoints from '../Base/breakpoints';
import colors from './colors';
import fonts from '../Base/fonts';
import fontSizes from '../Base/fontSizes';
import fontWeights from '../Base/fontWeights';
import shadow from '../Base/shadow';
import space from '../Base/space';

const darkTheme = {
  breakpoints,
  colors,
  fonts,
  fontSizes,
  fontWeights,
  shadow,
  space,
} as const;

export default darkTheme;
