export default {
  lighterWeight: '400',
  defaultWeight: '500',
  boldWeight: '700',
};
