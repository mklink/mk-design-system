export default {
  sm: '30em', // 480px
  md: '48em', // 768px
  lg: '64em', // 1024px
};
