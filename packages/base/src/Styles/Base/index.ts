import breakpoints from './breakpoints';
import colors from './colors';
import fonts from './fonts';
import fontSizes from './fontSizes';
import fontWeights from './fontWeights';
import shadow from './shadow';
import space from './space';

export { breakpoints, colors, fonts, fontSizes, fontWeights, shadow, space };

type Breakpoints = typeof breakpoints;
type Colors = typeof colors;
type Fonts = typeof fonts;
type FontSizes = typeof fontSizes;
type FontWeights = typeof fontWeights;
type Shadow = typeof shadow;
type Space = typeof space;

// Only need to export this once
export interface MKTheme {
  breakpoints: Breakpoints;
  colors: Colors;
  fonts: Fonts;
  fontSizes: FontSizes;
  fontWeights: FontWeights;
  shadow: Shadow;
  space: Space;
}
