export default {
  oneDP:
    '0 1px 3px 0 rgba(0, 0, 0, 0.20), 0 2px 1px 0 rgba(0, 0, 0, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.14)',
  twoDP:
    '0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.14)',
  threeDP:
    '0 1px 8px 0 rgba(0, 0, 0, 0.20), 0 3px 3px 0 rgba(0, 0, 0, 0.12), 0 3px 4px 0 rgba(0, 0, 0, 0.14)',
  fourDP:
    '0 2px 4px 0 rgba(0, 0, 0, 0.20), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 4px 5px 0 rgba(0, 0, 0, 0.14)',
  sixDP:
    '0 3px 5px 0 rgba(0, 0, 0, 0.20), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14)',
  eightDP:
    '0 5px 5px 0 rgba(0, 0, 0, 0.20), 0 3px 14px 0 rgba(0, 0, 0, 0.12), 0 8px 10px 0 rgba(0, 0, 0, 0.14)',
  nineDP:
    '0 5px 6px 0 rgba(0, 0, 0, 0.20), 0 3px 16px 0 rgba(0, 0, 0, 0.12), 0 9px 12px 0 rgba(0, 0, 0, 0.14)',
  twelveDP:
    '0 7px 8px 0 rgba(0, 0, 0, 0.20), 0 5px 22px 0 rgba(0, 0, 0, 0.12), 0 12px 17px 0 rgba(0, 0, 0, 0.14)',
  sixteenDP:
    '0 8px 10px 0 rgba(0, 0, 0, 0.20), 0 6px 30px 0 rgba(0, 0, 0, 0.12), 0 16px 24px 0 rgba(0, 0, 0, 0.14)',
  twentyFourDP:
    '0 11px 15px 0 rgba(0, 0, 0, 0.20), 0 9px 46px 0 rgba(0, 0, 0, 0.12), 0 24px 38px 0 rgba(0, 0, 0, 0.14)',
};
