/* ============================================================================
The name stands for rem + the corresponding px amount - this was done so devs do not have to convert the values
 ============================================================================== */

export default {
  rpx2: '0.125rem',
  rpx4: '0.25rem',
  rpx8: '0.5rem',
  rpx12: '0.75rem',
  rpx16: '1rem',
  rpx20: '1.25rem',
  rpx24: '1.5rem',
  rpx28: '1.75rem',
  rpx32: '2rem',
  rpx36: '2.25rem',
  rpx40: '2.5rem',
} as const;
