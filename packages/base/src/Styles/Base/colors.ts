// color names and values from design
const baseColors = {
  white: '#ffffff',
  black: '#000000',
  /* Light Shades */
  light100: '#f1f4ed',
  light200: '#e3eadc',
  light300: '#d6dfca',
  light400: '#c8d4b9',
  /* Light Accent */
  lightAccent100: '#98bcb3',
  lightAccent200: '#80aa9f',
  lightAccent300: '#67988b',
  lightAccent400: '#577e74',
  /* Primary Shades */
  primaryBrand100: '#97c6c3',
  primaryBrand200: '#7cb6b2',
  primaryBrand300: '#62a6a1',
  primaryBrand400: '#518c88',
  primaryBrand500: '#42716d',
  primaryBrand600: '#335553',
  /* Dark Accent */
  darkAccent100: '#537e77',
  darkAccent200: '#42645f',
  darkAccent300: '#324a46',
  darkAccent400: '#21302e',
  /* Dark Shades */
  dark100: '#303341',
  dark200: '#242631',
  dark300: '#181a20',
  dark400: '#0c0d10',
  /* Gray */
  gray100: '#eaeaea',
  gray200: '#d6d6d6',
  gray300: '#c2c2c2',
  gray400: '#8f8f8f',
  /* Health */
  successColor: '#53ac68',
  warningColor: '#d09c30',
  dangerColor: '#f44336',
};

const aliases = {
  // aliases
  primaryTextColor: baseColors.dark300,
  secondaryTextColor: baseColors.gray400,
  /* Primary and Secondary Colors */
  primaryColor: baseColors.primaryBrand300,
  secondaryColor: baseColors.dark100,
  /* Hover */
  hoverColor: baseColors.primaryBrand500,
  hoverTextColor: baseColors.white,
  hoverOutlinedBGColor: baseColors.light100,
  /* Focus */
  focusColor: baseColors.primaryBrand500,
  focusTextColor: baseColors.white,
  focusOutlinedBGColor: baseColors.light100,
  /* Active */
  activeColor: baseColors.primaryBrand600,
  activeTextColor: baseColors.white,
  activeOutlinedBGColor: baseColors.light200,
  /* Focus Outline */
  focusRingColor: baseColors.lightAccent100,
  /* error */
  errorColor: baseColors.dangerColor,
  /* Disabled Colors */
  disabledColor: baseColors.gray100,
  disabledTextColor: baseColors.gray300,
  /* Borders */
  borderColor: baseColors.gray300,
};

export default {
  ...baseColors,
  ...aliases,
};
