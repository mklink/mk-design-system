/* ============================================================================
The name stands for rem + the corresponding px amount - this was done so devs do not have to convert the values
 ============================================================================== */

export default {
  rpx12: "0.75rem",
  rpx14: "0.875rem",
  rpx16: "1rem",
  rpx18: "1.125rem",
  rpx20: "1.25rem",
  rpx22: "1.375rem",
  rpx24: "1.5rem",
  rpx26: "1.625rem",
  rpx28: "1.75rem",
  rpx30: "1.875rem",
  rpx32: "2rem",
  rpx34: "2.125rem",
  rpx36: "2.25rem",
  rpx38: "2.375rem",
  rpx40: "2.5rem",
};
