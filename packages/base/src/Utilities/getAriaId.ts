export interface GetAriaIdProps {
  helperText?: string;
  errorText?: string;
  errorId?: string;
  helperId?: string;
}

function getAriaId(ariaProps: GetAriaIdProps) {
  const { helperText, errorText, errorId, helperId } = ariaProps;
  if (helperText && errorText) {
    return `${errorId} ${helperId}`;
  }

  if (errorText) {
    return errorId;
  }

  if (helperText) {
    return helperId;
  }

  return undefined;
}

export default getAriaId;
