import * as React from 'react';
import { ThemeProvider as MKThemeProvider } from 'styled-components';

import { MKTheme } from '../Styles';

export interface ThemeProviderProps {
  children: React.ReactChild;
  theme: MKTheme;
}

const ThemeProvider: React.FC<ThemeProviderProps> = ({ children, theme }) => (
  <MKThemeProvider theme={theme}>{children}</MKThemeProvider>
);

export default ThemeProvider;
