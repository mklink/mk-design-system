## `@mklink/base`

### Required Dependencies

`@mklink/base` has the following peer dependencies which it expects all the projects using it to have.

- "react": "^17.x || 18.x",
- "react-dom": "^17.x || 18.x",
- "react-router-dom": "^5.x",
- "styled-components": "^6.0.0"
