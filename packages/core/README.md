## `@mklink/core`

### Required Dependencies

`@mklink/core` has the following peer dependencies which it expects all the projects using it to have.

- "@mklink/base": "^1.0.0",
- "react": "^16.8.x || 17.x",
- "react-dom": "^16.8.x || 17.x",
- "react-router-dom": "^5.x",
- "styled-components": "^6.0.0",
