import * as React from 'react';
import classNames from 'classnames';

import { BaseFontStyle } from '../Styles/typographyStyles';
import { HeadingProps } from './headingProps';

export type H3Props = HeadingProps;

export const H3 = React.forwardRef<HTMLHeadingElement, H3Props>(function H3(
  props,
  ref
) {
  const {
    children,
    className,
    mjs,
    qa,
    styleOf = 'headlineFive',
    ...rest
  } = props;
  return (
    <BaseFontStyle
      className={classNames(className, {
        'headline-one': styleOf === 'headlineOne',
        'headline-two': styleOf === 'headlineTwo',
        'headline-three': styleOf === 'headlineThree',
        'headline-four': styleOf === 'headlineFour',
        'headline-five': styleOf === 'headlineFive',
        'headline-six': styleOf === 'headlineSix',
        'headline-seven': styleOf === 'headlineSeven',
        'body-text-one': styleOf === 'bodyTextOne',
        'body-text-two': styleOf === 'bodyTextTwo',
        caption: styleOf === 'caption',
      })}
      as="h3"
      data-mjs={mjs}
      data-qa={qa}
      {...rest}
      ref={ref}
    >
      {children}
    </BaseFontStyle>
  );
});
