import * as React from 'react';
import classNames from 'classnames';

import { BaseFontStyle } from '../Styles/typographyStyles';
import { HeadingProps } from './headingProps';

export type H5Props = HeadingProps;

export const H5 = React.forwardRef<HTMLHeadingElement, H5Props>(function H5(
  props,
  ref
) {
  const {
    children,
    className,
    mjs,
    qa,
    styleOf = 'bodyTextOne',
    ...rest
  } = props;
  return (
    <BaseFontStyle
      className={classNames(className, {
        'headline-one': styleOf === 'headlineOne',
        'headline-two': styleOf === 'headlineTwo',
        'headline-three': styleOf === 'headlineThree',
        'headline-four': styleOf === 'headlineFour',
        'headline-five': styleOf === 'headlineFive',
        'headline-six': styleOf === 'headlineSix',
        'headline-seven': styleOf === 'headlineSeven',
        'body-text-one': styleOf === 'bodyTextOne',
        'body-text-two': styleOf === 'bodyTextTwo',
        caption: styleOf === 'caption',
      })}
      as="h5"
      data-mjs={mjs}
      data-qa={qa}
      {...rest}
      ref={ref}
    >
      {children}
    </BaseFontStyle>
  );
});
