import * as React from 'react';
import classNames from 'classnames';

import { BaseFontStyle } from '../Styles/typographyStyles';
import { HeadingProps } from './headingProps';

export type H4Props = HeadingProps;

export const H4 = React.forwardRef<HTMLHeadingElement, H4Props>(function H4(
  props,
  ref
) {
  const {
    children,
    className,
    mjs,
    qa,
    styleOf = 'headlineSeven',
    ...rest
  } = props;
  return (
    <BaseFontStyle
      className={classNames(className, {
        'headline-one': styleOf === 'headlineOne',
        'headline-two': styleOf === 'headlineTwo',
        'headline-three': styleOf === 'headlineThree',
        'headline-four': styleOf === 'headlineFour',
        'headline-five': styleOf === 'headlineFive',
        'headline-six': styleOf === 'headlineSix',
        'headline-seven': styleOf === 'headlineSeven',
        'body-text-one': styleOf === 'bodyTextOne',
        'body-text-two': styleOf === 'bodyTextTwo',
        caption: styleOf === 'caption',
      })}
      as="h4"
      data-mjs={mjs}
      data-qa={qa}
      {...rest}
      ref={ref}
    >
      {children}
    </BaseFontStyle>
  );
});
