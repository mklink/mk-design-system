import {  StyleOfProp } from "../types/typographyProps";

export interface HeadingProps {
  children?: React.ReactNode;
  className?: string;
  mjs?: string;
  qa?: string;
  styleOf?: StyleOfProp;
}
