import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { H3, H3Props } from "../H3";

describe("Component: H3", () => {
  function renderWithTheme(newProps?: H3Props) {
    const props = {
      children: "heading text",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <H3 {...props} />
      </ThemeProvider>
    );
  }

  it("should handle styleOf correctly", () => {
    renderWithTheme({ styleOf: "headlineThree" });
    const heading = screen.getByText("heading text");

    expect(heading).toHaveClass("headline-three");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const heading = screen.getByText("heading text");

    expect(heading).toHaveTextContent("heading text");
  });

  it("should pass the className correctly", () => {
    renderWithTheme();
    const heading = screen.getByText("heading text");

    expect(heading).toHaveClass("foo");
  });
});
