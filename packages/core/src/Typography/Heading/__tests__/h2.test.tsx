import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { H2, H2Props } from "../H2";

describe("Component: H2", () => {
  function renderWithTheme(newProps?: H2Props) {
    const props = {
      children: "heading text",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <H2 {...props} />
      </ThemeProvider>
    );
  }

  it("should handle styleOf correctly", () => {
    renderWithTheme({ styleOf: "headlineThree" });
    const heading = screen.getByText("heading text");

    expect(heading).toHaveClass("headline-three");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const heading = screen.getByText("heading text");

    expect(heading).toHaveTextContent("heading text");
  });

  it("should pass the className correctly", () => {
    renderWithTheme();
    const heading = screen.getByText("heading text");

    expect(heading).toHaveClass("foo");
  });
});
