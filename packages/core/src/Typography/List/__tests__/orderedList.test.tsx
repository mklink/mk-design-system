import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { OrderedList, OrderedListProps } from "../OrderedList";

describe("Component: OrderedList.js", () => {
  function renderWithTheme(newProps?: OrderedListProps) {
    const props = {
      children: "list item",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <OrderedList {...props}>
          <li>list item</li>
        </OrderedList>
      </ThemeProvider>
    );
  }

  it("should pass the className correctly", () => {
    renderWithTheme();
    const olist = screen.getByTestId("styled-orderedlist");

    expect(olist).toHaveClass("foo");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const olist = screen.getByText("list item");

    expect(olist).toHaveTextContent("list item");
  });
});
