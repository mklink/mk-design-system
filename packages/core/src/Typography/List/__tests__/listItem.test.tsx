import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { ListItem, ListItemProps } from "../ListItem";

describe("Component: ListItem.js", () => {
  function renderWithTheme(newProps?: ListItemProps) {
    const props = {
      children: "list item text",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <ListItem {...props} />
      </ThemeProvider>
    );
  }

  it("should pass the className correctly", () => {
    renderWithTheme();
    const list = screen.getByText("list item text");

    expect(list).toHaveClass("foo");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const list = screen.getByText("list item text");

    expect(list).toHaveTextContent("list item text");
  });
});
