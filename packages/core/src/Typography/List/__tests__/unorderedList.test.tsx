import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { UnorderedList, UnorderedListProps } from "../UnorderedList";

describe("Component: UnorderedList.js", () => {
  function renderWithTheme(newProps?: UnorderedListProps) {
    const props = {
      children: "paragraph text",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <UnorderedList {...props}>
          <li>list item</li>
        </UnorderedList>
      </ThemeProvider>
    );
  }

  it("should pass the className correctly", () => {
    renderWithTheme();
    const olist = screen.getByTestId("styled-unorderedlist");

    expect(olist).toHaveClass("foo");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const olist = screen.getByText("list item");

    expect(olist).toHaveTextContent("list item");
  });

  it("should handle noBullets prop correctly", () => {
    renderWithTheme({ noBullets: true });
    const olist = screen.getByTestId("styled-unorderedlist");

    expect(olist).toHaveStyle("padding-left: 0");
  });
});
