import * as React from 'react';

export interface ListItemProps {
  children: React.ReactNode;
}

export const ListItem = React.forwardRef<HTMLLIElement, ListItemProps>(
  function ListItem(props, ref) {
    const { children, ...rest } = props;
    return (
      <li {...rest} ref={ref}>
        {children}
      </li>
    );
  }
);
