import * as React from "react";
import styled from "styled-components";

interface StyleProps {
  listStyle?: string;
  noBullets?: boolean;
}

export interface UnorderedListProps extends StyleProps {
  children: React.ReactNode;
}

/* Styled system props placed before noBullest styles so it can wont override them */
const StyledUnorderedList = styled("ul")<StyleProps>`
  color: ${(props) => props.theme.colors.primaryTextColor};
  font-family: ${(props) => props.theme.fonts.primaryText};
  font-size: ${(props) => props.theme.fontSizes.rpx16};
  font-weight: ${(props) => props.theme.fontWeights.lighterWeight};
  list-style-type: ${(props) => (props.listStyle ? props.listStyle : "disc")};
  padding-left: ${(props) => props.theme.space.rpx20};

  /* VoiceOver accessibility improvement */
  &.no-bullets {
    padding-left: 0;

    li {
      list-style-type: none;
    }

    /* add zero-width space to have VoiceOver announce content as list */
    li:before {
      content: "\\200B";
    }
  }
`;

export const UnorderedList = React.forwardRef<
  HTMLUListElement,
  UnorderedListProps
>(function UnorderedList(props, ref) {
  const { children, ...rest } = props;
  return (
    <StyledUnorderedList data-testid="styled-unorderedlist" {...rest} ref={ref}>
      {children}
    </StyledUnorderedList>
  );
});
