import * as React from 'react';
import {  styled } from 'styled-components';


export interface OrderedListProps {
  children: React.ReactNode;
}

const StyledOrderedList = styled('ol')`
  color: ${props => props.theme.colors.primaryTextColor};
  font-family: ${props => props.theme.fonts.primaryText};
  font-size: ${props => props.theme.fontSizes.rpx16};
  font-weight: ${props => props.theme.fontWeights.lighterWeight};
  padding-left: ${props => props.theme.space.rpx20};
`;

export const OrderedList = React.forwardRef<
  Omit<HTMLOListElement, 'color'>,
  OrderedListProps
>(function OrderedList(props, ref) {
  const { children, ...rest } = props;
  return (
    <StyledOrderedList data-testid="styled-orderedlist" {...rest} ref={ref}>
      {children}
    </StyledOrderedList>
  );
});
