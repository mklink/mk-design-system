import styled from "styled-components";

export const BaseFontStyle = styled("p")`
  font-family: ${(props) => props.theme.fonts.primaryText};
  color: ${(props) => props.theme.colors.primaryTextColor};
  letter-spacing: 0;

  &.headline-one {
    font-size: ${(props) => props.theme.fontSizes.rpx40};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.2;
  }
  &.headline-two {
    font-size: ${(props) => props.theme.fontSizes.rpx32};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.25;
  }
  &.headline-three {
    font-size: ${(props) => props.theme.fontSizes.rpx28};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.285;
  }
  &.headline-four {
    font-size: ${(props) => props.theme.fontSizes.rpx24};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.35;
  }
  &.headline-five {
    font-size: ${(props) => props.theme.fontSizes.rpx20};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.2;
  }
  &.headline-six {
    font-size: ${(props) => props.theme.fontSizes.rpx18};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.35;
  }
  &.headline-seven {
    font-size: ${(props) => props.theme.fontSizes.rpx16};
    font-weight: ${(props) => props.theme.fontWeights.boldWeight};
    line-height: 1.25;
  }
  &.body-text-one {
    font-size: ${(props) => props.theme.fontSizes.rpx16};
    font-weight: ${(props) => props.theme.fontWeights.lighterWeight};
    line-height: 1.5;
  }
  &.body-text-two {
    font-size: ${(props) => props.theme.fontSizes.rpx14};
    font-weight: ${(props) => props.theme.fontWeights.defaultWeight};
    line-height: 1.4285;
  }
  &.caption {
    font-size: ${(props) => props.theme.fontSizes.rpx12};
    font-weight: ${(props) => props.theme.fontWeights.defaultWeight};
    line-height: 1.35;
  }
`;
