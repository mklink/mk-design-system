import * as React from "react";
import classNames from "classnames";

import { BaseFontStyle } from "../Styles/typographyStyles";
import { StyleOfProp } from "../types/typographyProps";

export interface ParagraphProps {
  children?: React.ReactNode;
  className?: string;
  mjs?: string;
  qa?: string;
  styleOf?: StyleOfProp;
}

export const Paragraph = React.forwardRef<HTMLParagraphElement, ParagraphProps>(
  function Paragraph(props, ref) {
    const {
      children,
      className,
      mjs,
      qa,
      styleOf = "bodyTextOne",
      ...rest
    } = props;
    return (
      <BaseFontStyle
        className={classNames(className, {
          "headline-one": styleOf === "headlineOne",
          "headline-two": styleOf === "headlineTwo",
          "headline-three": styleOf === "headlineThree",
          "headline-four": styleOf === "headlineFour",
          "headline-five": styleOf === "headlineFive",
          "headline-six": styleOf === "headlineSix",
          "headline-seven": styleOf === "headlineSeven",
          "body-text-one": styleOf === "bodyTextOne",
          "body-text-two": styleOf === "bodyTextTwo",
          caption: styleOf === "caption",
        })}
        data-mjs={mjs}
        data-qa={qa}
        {...rest}
        ref={ref}
      >
        {children}
      </BaseFontStyle>
    );
  }
);
