import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { Paragraph, ParagraphProps } from "../Paragraph";

describe("Component: Paragraph.js", () => {
  function renderWithTheme(newProps?: ParagraphProps) {
    const props = {
      children: "paragraph text",
      className: "foo",
      ...newProps,
    };
    return render(
      <ThemeProvider theme={baseTheme}>
        <Paragraph {...props} />
      </ThemeProvider>
    );
  }

  it("should handle styleOf correctly", () => {
    renderWithTheme({ styleOf: "headlineThree" });
    const paragraph = screen.getByText("paragraph text");

    expect(paragraph).toHaveClass("headline-three");
  });

  it("should have the correct children", () => {
    renderWithTheme();
    const paragraph = screen.getByText("paragraph text");

    expect(paragraph).toHaveTextContent("paragraph text");
  });

  it("should pass the className correctly", () => {
    renderWithTheme();
    const paragraph = screen.getByText("paragraph text");

    expect(paragraph).toHaveClass("foo");
  });
});
