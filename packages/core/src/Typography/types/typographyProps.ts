export type StyleOfProp =
  | "Custom"
  | "headlineOne"
  | "headlineTwo"
  | "headlineThree"
  | "headlineFour"
  | "headlineFive"
  | "headlineSix"
  | "headlineSeven"
  | "bodyTextOne"
  | "bodyTextTwo"
  | "caption";
