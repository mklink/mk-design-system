import * as React from "react";
import styled from "styled-components";
import { FocusStyles } from "@mklink/base";

interface StyleProps {
  isSmall?: boolean;
}

export interface BaseButtonProps extends StyleProps {
  isDisabled?: boolean;
  isProcessing?: boolean;
  mjs?: string;
  qa?: string;
}

const StyledBaseButton = styled("button")<StyleProps>`
  /* to keep height the same across all styles a default is set */
  border: 2px solid transparent;
  border-radius: ${(props) => (props.isSmall ? "0.25rem" : "0.375rem")};
  cursor: pointer;
  font-family: ${(props) => props.theme.fonts.primaryText};
  font-size: ${(props) => props.theme.fontSizes.rpx16};
  font-weight: ${(props) => props.theme.fontWeights.boldWeight};
  letter-spacing: 0.012rem;
  /* specify standard line height unit - when a button gets display: flex it becomes an inline element
  thus it will get taller as inline elements don't recognize line-height
  to match design spec for height padding is used instead line-height */
  line-height: 1.5;
  min-width: ${(props) => (props.isSmall ? "4rem" : "5rem")};
  /* padding accounts for 2px border */
  padding: ${(props) =>
    props.isSmall ? "0.25rem 0.375rem" : "0.625rem 0.875rem"};
  text-align: center;

  /* General order is link(a), :visited, :hover, :focus, :active */
  /* Removes browser outline on hover and active states */
  &:hover,
  &:focus,
  &:active {
    outline: none;
  }

  /* Remove dotted box outlining button text on focus in Firefox */
  &::-moz-focus-inner {
    border: 0;
  }

  /* Adds the focus state */
  ${FocusStyles}

  &:disabled,
  &[disabled] {
    box-shadow: none;
    cursor: not-allowed;
    outline: none;
  }
`;

export const BaseButton = React.forwardRef<HTMLButtonElement, BaseButtonProps>(
  function BaseButton(props, ref) {
    const { isDisabled, isProcessing, mjs, qa, ...rest } = props;

    return (
      <StyledBaseButton
        data-qa={qa}
        data-mjs={mjs}
        disabled={isDisabled || isProcessing}
        {...rest}
        ref={ref}
      />
    );
  }
);
