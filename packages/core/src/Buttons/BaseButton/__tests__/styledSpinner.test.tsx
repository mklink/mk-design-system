import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { StyledSpinner } from "../StyledSpinner";

describe("Component: StyledSpinner", () => {
  function renderWithTheme() {
    const props = {
      isSmall: true,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <StyledSpinner {...props} data-testid="styled-spinner" />
      </ThemeProvider>
    );
  }

  it("Should render the small spinner", () => {
    renderWithTheme();

    const spinner = screen.getByTestId("styled-spinner");
    expect(spinner).toHaveStyle("width: 1.125rem");
  });
});
