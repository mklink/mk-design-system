import * as React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { BaseButton, BaseButtonProps } from "../BaseButton";

describe("Component: BaseButton", () => {
  function renderWithTheme(newProps?: BaseButtonProps) {
    const props = {
      className: "foo",
      mjs: "foo-mjs",
      qa: "foo-qa",
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <BaseButton {...props}>Child</BaseButton>
      </ThemeProvider>
    );
  }

  it("should call onClick when the button is clicked", () => {
    const onClick = jest.fn();
    renderWithTheme({ onClick });

    fireEvent.click(screen.getByText("Child"));
    expect(onClick).toHaveBeenCalled();
  });

  it("Should add disabled attribute when disabled", () => {
    renderWithTheme({ isDisabled: true });
    const button = screen.getByText("Child");

    expect(button).toBeDisabled();
  });

  it("Should add disabled attribute when processing", () => {
    renderWithTheme({ isProcessing: true });
    const button = screen.getByText("Child");

    expect(button).toBeDisabled();
  });
});
