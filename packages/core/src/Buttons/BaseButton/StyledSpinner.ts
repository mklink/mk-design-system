import { keyframes } from "styled-components";
import styled from "styled-components";

interface StyleProps {
  isSmall?: boolean;
}

const load3 = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const StyledSpinner = styled.span<StyleProps>`
  height: ${(props) => (props.isSmall ? "1.125rem" : "1.5rem")};
  margin-right: 0.5rem;
  position: relative;
  width: ${(props) => (props.isSmall ? "1.125rem" : "1.5rem")};

  /* creates the spinners solid leading edge */
  :before {
    animation: ${load3} 2s infinite linear;
    content: "";
    /* border-color: set in individual component */
    border: 0.125rem solid transparent;
    border-radius: 50%;
    height: ${(props) => (props.isSmall ? "1.125rem" : "1.5rem")};
    left: 0;
    position: absolute;
    width: ${(props) => (props.isSmall ? "1.125rem" : "1.5rem")};
  }
`;
