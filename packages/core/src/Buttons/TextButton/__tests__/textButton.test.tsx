import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { TextButton } from "../TextButton";

describe("Component: TextButton", () => {
  function renderWithTheme() {
    const props = {
      className: "foo",
      isSmall: true,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <TextButton {...props}>Button</TextButton>
      </ThemeProvider>
    );
  }

  it("should pass the correct props", () => {
    renderWithTheme();

    const button = screen.getByText("Button");

    expect(button).toHaveClass("foo");
    expect(button).toHaveStyle("padding: 0.25rem 0.375rem");
  });
});
