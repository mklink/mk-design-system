import * as React from "react";
import "focus-visible";
import styled from "styled-components";

import { BaseButton, BaseButtonProps } from "../BaseButton";

export interface TextButtonProps extends BaseButtonProps {
  children: React.ReactNode;
}

const StyledTextButton = styled(BaseButton)`
  background: none;
  color: ${(props) => props.theme.colors.primaryActionColor};

  /* Adds styles for hover */
  &:hover {
    background-color: ${(props) => props.theme.colors.hoverOutlinedColor};
    color: ${(props) => props.theme.colors.hoverColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.hoverColor};
      }
    }
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseButton from Focus Styles */
  &.focus-visible {
    background-color: ${(props) => props.theme.colors.focusOutlinedColor};
    color: ${(props) => props.theme.colors.focusColor};
    /* default outline color is reset on buttons without a border */
    outline-color: ${(props) => props.theme.colors.focusRingLightColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.focusColor};
      }
    }
  }

  /* Adds the pressed state */
  &:active {
    background-color: ${(props) => props.theme.colors.activeOutlinedColor};
    color: ${(props) => props.theme.colors.activeColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.activeColor};
      }
    }
  }

  &:disabled,
  &[disabled] {
    /* Base Button contains common disabled styles */
    background: none;
    color: ${(props) => props.theme.colors.primaryActionColor};
    opacity: 30%;
  }
`;

export const TextButton = React.forwardRef<HTMLButtonElement, TextButtonProps>(
  function TextButton(props, ref) {
    const { children, ...rest } = props;
    return (
      <StyledTextButton {...rest} ref={ref}>
        {children}
      </StyledTextButton>
    );
  }
);
