import * as React from "react";
import "focus-visible";
import styled from "styled-components";

import {
  BaseButton,
  BaseButtonProps,
  StyledSpinner as Spinner,
} from "../BaseButton";

export interface OutlinedButtonProps extends BaseButtonProps {
  children?: React.ReactNode;
}

const StyledOutlinedButton = styled(BaseButton)`
  background-color: ${(props) => props.theme.colors.outlinedColor};
  border-color: ${(props) => props.theme.colors.primaryActionColor};
  color: ${(props) => props.theme.colors.primaryActionColor};

  /* Adds styles for hover */
  &:hover {
    background-color: ${(props) => props.theme.colors.hoverOutlinedColor};
    border-color: ${(props) => props.theme.colors.hoverColor};
    color: ${(props) => props.theme.colors.hoverColor};
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseButton from Focus Styles */
  &.focus-visible {
    background-color: ${(props) => props.theme.colors.focusOutlinedColor};
    border-color: ${(props) => props.theme.colors.focusColor};
    color: ${(props) => props.theme.colors.focusColor};
    /* default outline color is reset on outlined buttons */
    outline-color: ${(props) => props.theme.colors.focusRingOutlinedColor};
  }

  /* Adds the pressed state */
  &:active {
    background-color: ${(props) => props.theme.colors.activeOutlinedColor};
    border-color: ${(props) => props.theme.colors.activeColor};
    color: ${(props) => props.theme.colors.activeColor};
  }

  &:disabled,
  &[disabled] {
    /* Base Button contains common disabled styles */
    background-color: ${(props) => props.theme.colors.outlinedColor};
    border-color: ${(props) => props.theme.colors.primaryActionColor};
    color: ${(props) => props.theme.colors.primaryActionColor};
    /* positions label and spinner on processing button */
    display: ${(props) => (props.isProcessing ? "flex" : "")};
    align-items: ${(props) => (props.isProcessing ? "center" : "")};
    justify-content: ${(props) => (props.isProcessing ? "center" : "")};
    opacity: 30%;
  }
`;

const StyledSpinner = styled(Spinner)`
  /* creates the spinners solid leading edge */
  :before {
    border-top-color: ${(props) => props.theme.colors.primaryActionColor};
  }
`;

export const OutlinedButton = React.forwardRef<
  HTMLButtonElement,
  OutlinedButtonProps
>(function OutlinedButton(props, ref) {
  const { children, isProcessing, isSmall, ...rest } = props;

  return (
    <StyledOutlinedButton
      isSmall={isSmall}
      isProcessing={isProcessing}
      {...rest}
      ref={ref}
    >
      {isProcessing && <StyledSpinner isSmall={isSmall} />}
      {children}
    </StyledOutlinedButton>
  );
});
