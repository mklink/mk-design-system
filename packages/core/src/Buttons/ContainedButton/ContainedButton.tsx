import * as React from "react";
import "focus-visible";
import styled from "styled-components";

import {
  BaseButton,
  BaseButtonProps,
  StyledSpinner as Spinner,
} from "../BaseButton";

export interface ContainedButtonProps extends BaseButtonProps {
  children?: React.ReactNode;
}

const StyledContainedButton = styled(BaseButton)<ContainedButtonProps>`
  background-color: ${(props) => props.theme.colors.primaryActionColor};
  border-color: ${(props) => props.theme.colors.primaryActionColor};
  color: ${(props) => props.theme.colors.primaryButtonTextColor};

  /* Adds styles for hover and focus (includes keyboard which is removed with :focus:not(:focus-visible) - FocusStyles from BaseButton) */
  &:hover {
    background-color: ${(props) => props.theme.colors.hoverBGColor};
    border-color: ${(props) => props.theme.colors.hoverBGColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseLink from Focus Styles */
  &.focus-visible {
    background-color: ${(props) => props.theme.colors.focusColor};
    border-color: ${(props) => props.theme.colors.focusColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  /* Adds the pressed state */
  &:active {
    background-color: ${(props) => props.theme.colors.activeBGColor};
    border-color: ${(props) => props.theme.colors.activeBGColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  &:disabled,
  &[disabled] {
    /* Base Button contains common disabled styles */
    background-color: ${(props) => props.theme.colors.primaryActionColor};
    border-color: ${(props) => props.theme.colors.primaryActionColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
    /* positions label and spinner on isProcessing button */
    display: ${(props) => (props.isProcessing ? "flex" : "")};
    align-items: ${(props) => (props.isProcessing ? "center" : "")};
    justify-content: ${(props) => (props.isProcessing ? "center" : "")};
    opacity: 30%;
  }
`;

const StyledSpinner = styled(Spinner)`
  /* creates the spinners solid leading edge */
  :before {
    border-top-color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }
`;

export const ContainedButton = React.forwardRef<
  HTMLButtonElement,
  ContainedButtonProps
>(function ContainedButton(props, ref) {
  const { children, isProcessing, isSmall, ...rest } = props;

  return (
    <StyledContainedButton
      isSmall={isSmall}
      isProcessing={isProcessing}
      {...rest}
      ref={ref}
    >
      {isProcessing && <StyledSpinner isSmall={isSmall} />}
      {children}
    </StyledContainedButton>
  );
});
