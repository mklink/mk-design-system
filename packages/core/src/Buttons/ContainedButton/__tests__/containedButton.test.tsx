import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { ContainedButton, ContainedButtonProps } from "../ContainedButton";

describe("Component: ContainedButton", () => {
  function renderWithTheme(newProps?: ContainedButtonProps) {
    const props = {
      className: "foo",
      isSmall: true,
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <ContainedButton {...props}>Button</ContainedButton>
      </ThemeProvider>
    );
  }

  it("Should pass the correct props", () => {
    renderWithTheme();

    const button = screen.getByText("Button");

    expect(button).toHaveClass("foo");
    expect(button).toHaveStyle("padding: 0.25rem 0.375rem");
  });

  it("Should render processing button", () => {
    renderWithTheme({ isProcessing: true });

    const button = screen.getByText("Button");

    expect(button).toHaveStyle("display: flex");
    expect(button).toBeDisabled();
  });
});
