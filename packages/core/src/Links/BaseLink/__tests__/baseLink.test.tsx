import * as React from "react";
import { Link, MemoryRouter } from "react-router-dom";
import { render, fireEvent, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { BaseLink, BaseLinkProps } from "../BaseLink";

describe("Component: BaseLink", () => {
  function renderWithTheme(newProps?: BaseLinkProps) {
    const props = {
      className: "foo",
      mjs: "foo-mjs",
      qa: "foo-qa",
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <MemoryRouter>
          <BaseLink {...props}>Child</BaseLink>
        </MemoryRouter>
      </ThemeProvider>
    );
  }

  it("Should call onClick when the button is clicked", () => {
    const onClick = jest.fn();
    renderWithTheme({ onClick });

    fireEvent.click(screen.getByText("Child"));
    expect(onClick).toHaveBeenCalled();
  });

  it("Should have small styles", () => {
    renderWithTheme({ isSmall: true });
    const link = screen.getByText("Child");

    expect(link).toHaveStyle("padding: 0.25rem 0.375rem");
  });

  it('Should allow for "to" prop to be an object', () => {
    const toObject = {
      pathname: "/new-page",
      state: { from: "/current-page" },
    };

    renderWithTheme({
      to: toObject,
      linkType: Link,
    });
    const link = screen.getByText("Child");

    expect(link).toHaveAttribute("href", toObject.pathname);
  });
});
