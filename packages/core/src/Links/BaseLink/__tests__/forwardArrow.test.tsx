import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { ForwardArrow } from "../ForwardArrow";

describe("Component: ForwardArrow", () => {
  function renderWithTheme() {
    return render(
      <ThemeProvider theme={baseTheme}>
        <ForwardArrow />
      </ThemeProvider>
    );
  }

  it("should render without errors", () => {
    renderWithTheme();
    const svg = screen.getByTestId("styled-forward-arrow");

    expect(svg).toHaveClass("forward-arrow");
  });
});
