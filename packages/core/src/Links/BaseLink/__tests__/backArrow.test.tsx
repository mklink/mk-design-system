import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { BackArrow } from "../BackArrow";

describe("Component: BackArrow", () => {
  function renderWithTheme() {
    return render(
      <ThemeProvider theme={baseTheme}>
        <BackArrow />
      </ThemeProvider>
    );
  }

  it("should render without errors", () => {
    renderWithTheme();
    const svg = screen.getByTestId("styled-back-arrow");

    expect(svg).toHaveClass("back-arrow");
  });
});
