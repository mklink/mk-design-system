import * as React from 'react';

export const ForwardArrow = () => (
  <svg
    aria-hidden="true"
    className="forward-arrow"
    height="0.813em"
    width="0.5em"
    viewBox="0 0 8 13"
    focusable="false"
    data-testid="styled-forward-arrow"
  >
    <path
      d="M5.41,6.5.33,11.27a.94.94,0,0,0,0,1.38,1.09,1.09,0,0,0,1.47,0l0,
        0L7.58,7.23a1,1,0,0,0,0-1.45L1.75.31A1.09,1.09,0,0,0,.28.35a1,1,0,0,0,0,1.34L5.41,6.5Z"
    />
  </svg>
);
