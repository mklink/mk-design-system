import * as React from "react";
import { NavLinkProps } from "react-router-dom";
import styled from "styled-components";
import { FocusStyles } from "@mklink/base";

interface StyleProps {
  isSmall?: boolean;
}

type NavProps = Omit<NavLinkProps, "className" | "to" | "style">;

export interface BaseLinkProps extends NavProps {
  href?: string;
  isDownload?: boolean;
  isSmall?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  linkType?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
  mjs?: string;
  qa?: string;
  target?: string;
  to?:
    | string
    | {
        pathname?: string;
        search?: string;
        hash?: string;
        state?: { [key: string]: unknown };
      };
}

const StyledBaseLink = styled("a")<StyleProps>`
  /* to keep height the same across all styles a default is set */
  border: 2px solid transparent;
  border-radius: ${(props) => (props.isSmall ? "0.25rem" : "0.375rem")};
  color: ${(props) => props.theme.colors.primaryActionColor};
  font-family: ${(props) => props.theme.fonts.primaryText};
  font-size: ${(props) => props.theme.fontSizes.rpx16};
  font-weight: ${(props) => props.theme.fontWeights.boldWeight};
  letter-spacing: 0.012rem;
  line-height: 1.5;
  min-width: ${(props) => (props.isSmall ? "4rem" : "5rem")};
  padding: ${(props) =>
    props.isSmall ? "0.25rem 0.375rem" : "0.625rem 0.875rem"};
  text-decoration: none;

  /* General order is link(a), :visited, :hover, :focus, :active */
  /* Removes browser outline on hover and active states */
  &:hover,
  &:focus,
  &:active {
    outline: none;
  }

  /* reset for webkit which adds 1px offset */
  &:focus-visible {
    outline-offset: 0;
  }

  ${FocusStyles}
`;

export const BaseLink = React.forwardRef<HTMLAnchorElement, BaseLinkProps>(
  function BaseLink(props, ref) {
    const { isDownload, linkType, mjs, qa, ...rest } = props;

    return (
      <StyledBaseLink
        as={linkType}
        data-qa={qa}
        data-mjs={mjs}
        download={isDownload}
        {...rest}
        ref={ref}
      />
    );
  }
);
