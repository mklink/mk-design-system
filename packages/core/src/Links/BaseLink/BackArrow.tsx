import * as React from 'react';

export const BackArrow = () => (
  <svg
    aria-hidden="true"
    className="back-arrow"
    height="0.813em"
    width="0.5em"
    viewBox="0 0 8 13"
    focusable="false"
    data-testid="styled-back-arrow"
  >
    <path
      d="M2.48,6.5,7.56,1.73A.93.93,0,0,0,7.6.35a1.09,1.09,0,0,0-1.47,0s0,
      0,0,0L.32,5.77a1,1,0,0,0,0,1.45l5.82,5.47a1.09,1.09,0,0,0,1.47,0,.93.93,0,0,0,0-1.34L2.48,6.5Z"
    />
  </svg>
);
