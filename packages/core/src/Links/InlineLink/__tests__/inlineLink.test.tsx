import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { InlineLink, InlineLinkProps } from "../InlineLink";

describe("Component: InlineLink", () => {
  function renderWithTheme(newProps?: InlineLinkProps) {
    const props = {
      className: "foo",
      mjs: "foo-mjs",
      qa: "foo-qa",
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <InlineLink {...props}>Child</InlineLink>
      </ThemeProvider>
    );
  }

  it("Should pass the correct props", () => {
    renderWithTheme();
    const link = screen.getByText("Child");

    expect(link).toHaveClass("foo");
    expect(link).toHaveAttribute("data-qa");
    expect(link).toHaveAttribute("data-mjs");
  });

  it("Should add hidden text", () => {
    renderWithTheme({ target: "_blank" });
    const link = screen.getByText("Child");
    const span = screen.getByTestId("styled-text");

    expect(link).toContainElement(span);
  });
});
