import * as React from "react";
import "focus-visible";
import styled from "styled-components";

import { BaseLink, BaseLinkProps } from "../BaseLink";

export interface InlineLinkProps extends BaseLinkProps {
  children: React.ReactNode;
  graphic?: React.ReactNode;
}

const StyledLink = styled(BaseLink)`
  /* height doesn't matter here so use the default standard of 1.5 */
  line-height: 1.5;
  min-width: auto;
  padding: 0;
  text-decoration: underline;

  svg {
    margin-top: -4px;
    margin-right: 0.313rem;

    path {
      fill: ${(props) => props.theme.colors.primaryActionColor};
    }
  }

  /* Adds styles for hover */
  &:hover {
    color: ${(props) => props.theme.colors.hoverColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.hoverColor};
      }
    }
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseLink from Focus Styles */
  &.focus-visible {
    color: ${(props) => props.theme.colors.focusColor};
    /* adds a litte space since there is no padding */
    outline-offset: 1px;

    svg {
      path {
        fill: ${(props) => props.theme.colors.focusColor};
      }
    }
  }

  /* Adds the pressed state */
  &:active {
    color: ${(props) => props.theme.colors.activeColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.activeColor};
      }
    }
  }
`;

export const InlineLink = React.forwardRef<HTMLAnchorElement, InlineLinkProps>(
  function InlineLink(props, ref) {
    const { children, graphic, target, ...rest } = props;

    return (
      <StyledLink target={target} {...rest} ref={ref}>
        {graphic}

        {children}

        {target === "_blank" && (
          <span data-testid="styled-text" className="visually-hidden">
            , opens in new window
          </span>
        )}
      </StyledLink>
    );
  }
);
