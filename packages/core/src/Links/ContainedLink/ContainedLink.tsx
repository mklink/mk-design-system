import * as React from "react";
import "focus-visible";
import styled from "styled-components";
import { BaseLink, BaseLinkProps, BackArrow, ForwardArrow } from "../BaseLink";

export interface ContainedLinkProps extends BaseLinkProps {
  children: React.ReactNode;
  isBackLink?: boolean;
  isForwardLink?: boolean;
}

const StyledLink = styled(BaseLink)`
  background-color: ${(props) => props.theme.colors.primaryActionColor};
  border-color: ${(props) => props.theme.colors.primaryActionColor};
  color: ${(props) => props.theme.colors.primaryButtonTextColor};
  text-align: center;

  /* Adds styles for hover */
  &:hover {
    background-color: ${(props) => props.theme.colors.hoverBGColor};
    border-color: ${(props) => props.theme.colors.hoverBGColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseLink from Focus Styles */
  &.focus-visible {
    background-color: ${(props) => props.theme.colors.focusColor};
    border-color: ${(props) => props.theme.colors.focusColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  /* Adds the pressed state */
  &:active {
    background-color: ${(props) => props.theme.colors.activeBGColor};
    border-color: ${(props) => props.theme.colors.activeBGColor};
    color: ${(props) => props.theme.colors.primaryButtonTextColor};
  }

  /* Styles the forward and back arrow */
  svg {
    display: inline-block;
    vertical-align: initial;

    &.back-arrow {
      margin-right: 0.625rem;
    }
    &.forward-arrow {
      margin-left: 0.625rem;
    }

    path {
      fill: ${(props) => props.theme.colors.primaryButtonTextColor};
    }
  }
`;

export const ContainedLink = React.forwardRef<
  HTMLAnchorElement,
  ContainedLinkProps
>(function ContainedLink(props, ref) {
  const { children, isBackLink, isForwardLink, target, ...rest } = props;
  return (
    <StyledLink target={target} {...rest} ref={ref}>
      {isBackLink && <BackArrow />}

      {children}

      {target === "_blank" && (
        <span data-testid="styled-text" className="visually-hidden">
          , opens in new window
        </span>
      )}

      {isForwardLink && <ForwardArrow />}
    </StyledLink>
  );
});
