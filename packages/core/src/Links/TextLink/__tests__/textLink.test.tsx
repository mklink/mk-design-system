import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";
import { TextLink, TextLinkProps } from "../TextLink";

describe("Component: TextLink", () => {
  function renderWithTheme(newProps?: TextLinkProps) {
    const props = {
      className: "foo",
      mjs: "foo-mjs",
      qa: "foo-qa",
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <TextLink {...props}>Child</TextLink>
      </ThemeProvider>
    );
  }

  it("Should pass the correct props", () => {
    renderWithTheme();
    const link = screen.getByText("Child");

    expect(link).toHaveClass("foo");
    expect(link).toHaveAttribute("data-qa");
    expect(link).toHaveAttribute("data-mjs");
  });

  it("Should add hidden text", () => {
    renderWithTheme({ target: "_blank" });
    const link = screen.getByText("Child");
    const span = screen.getByTestId("styled-text");

    expect(link).toContainElement(span);
  });

  it("Should have back arrow", () => {
    renderWithTheme({
      isBackLink: true,
    });
    const link = screen.getByText("Child");
    const svg = screen.getByTestId("styled-back-arrow");

    expect(link).toContainElement(svg);
  });

  it("Should have forward arrow", () => {
    renderWithTheme({
      isForwardLink: true,
    });
    const link = screen.getByText("Child");
    const svg = screen.getByTestId("styled-forward-arrow");

    expect(link).toContainElement(svg);
  });
});
