import * as React from "react";
import "focus-visible";
import styled from "styled-components";
import { BaseLink, BaseLinkProps, BackArrow, ForwardArrow } from "../BaseLink";

export interface OutlinedLinkProps extends BaseLinkProps {
  children: React.ReactNode;
  isBackLink?: boolean;
  isForwardLink?: boolean;
}

const StyledLink = styled(BaseLink)`
  background-color: ${(props) => props.theme.colors.outlinedColor};
  border-color: ${(props) => props.theme.colors.primaryActionColor};
  color: ${(props) => props.theme.colors.primaryActionColor};
  text-align: center;

  /* Adds styles for hover */
  &:hover {
    background-color: ${(props) => props.theme.colors.hoverOutlinedColor};
    border-color: ${(props) => props.theme.colors.hoverColor};
    color: ${(props) => props.theme.colors.hoverColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.hoverColor};
      }
    }
  }

  /* Additional Keyboard Focus - shared outline styles are set in BaseLink from Focus Styles */
  &.focus-visible {
    background-color: ${(props) => props.theme.colors.focusOutlinedColor};
    border-color: ${(props) => props.theme.colors.focusColor};
    color: ${(props) => props.theme.colors.focusColor};
    /* default outline color is reset on outlined links */
    outline-color: ${(props) => props.theme.colors.focusRingOutlinedColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.focusColor};
      }
    }
  }

  /* Adds the pressed state */
  &:active {
    background-color: ${(props) => props.theme.colors.activeOutlinedColor};
    border-color: ${(props) => props.theme.colors.activeColor};
    color: ${(props) => props.theme.colors.activeColor};

    svg {
      path {
        fill: ${(props) => props.theme.colors.activeColor};
      }
    }
  }

  /* Styles the forward and back arrow */
  svg {
    display: inline-block;
    vertical-align: initial;

    &.back-arrow {
      margin-right: 0.625rem;
    }
    &.forward-arrow {
      margin-left: 0.625rem;
    }

    path {
      fill: ${(props) => props.theme.colors.primaryActionColor};
    }
  }
`;

export const OutlinedLink = React.forwardRef<
  HTMLAnchorElement,
  OutlinedLinkProps
>(function OutlinedLink(props, ref) {
  const { children, isBackLink, isForwardLink, target, ...rest } = props;

  return (
    <StyledLink target={target} {...rest} ref={ref}>
      {isBackLink && <BackArrow />}

      {children}

      {target === "_blank" && (
        <span data-testid="styled-text" className="visually-hidden">
          , opens in new window
        </span>
      )}

      {isForwardLink && <ForwardArrow />}
    </StyledLink>
  );
});
