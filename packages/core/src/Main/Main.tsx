import * as React from "react";
import styled from "styled-components";

export interface MainProps {
  children?: React.ReactNode;
}

const StyledMain = styled("main")`
  /* added to support IE which doesn't recognize html main element */
  display: block;

  &:hover,
  &:focus,
  &:active {
    outline: none;
  }
`;

const handleBlur = () => {
  document.getElementById("mainContent")?.removeAttribute("tabindex");
};

export const Main = React.forwardRef<HTMLElement, MainProps>(function Main(
  props,
  ref
) {
  const { children, ...rest } = props;

  return (
    <StyledMain
      id="mainContent" // added for skip link target
      onBlur={handleBlur}
      role="main" // added to support IE which doesn't recognize html main element
      {...rest}
      ref={ref}
    >
      {children}
    </StyledMain>
  );
});
