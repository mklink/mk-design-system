import * as React from "react";
import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import { baseTheme } from "@mklink/base";

import { Main, MainProps } from "../Main";

describe("Component: Main.js", () => {
  function renderWithTheme(newProps?: MainProps) {
    const props = {
      className: "class-name",
      ...newProps,
    };

    return render(
      <ThemeProvider theme={baseTheme}>
        <Main {...props}>App Content</Main>
      </ThemeProvider>
    );
  }

  it("should pass the className correctly", () => {
    renderWithTheme();

    screen.getByRole("main");
  });
});
